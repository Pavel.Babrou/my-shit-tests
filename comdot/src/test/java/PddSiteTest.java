import Pages.PddSite;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.security.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class PddSiteTest {
    WebDriver webDriver;
    PddSite webSite;
    Actions builder;
    WebDriverWait wait;

    @Before
    public void preCondition() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Maven\\chromedriver.exe");
        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 30, 300);
        webSite = new PddSite(webDriver);
        builder = new Actions(webDriver);

        webDriver.manage().window().maximize();

        WebDriverWait wait = new WebDriverWait(webDriver, 30,300);

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Action keyPPressed = builder.sendKeys("p").build();
        Action keyDPressed = builder.sendKeys("d").build();
        Action keyDotPressed = builder.sendKeys(".").build();
        Action keyBPressed = builder.sendKeys("b").build();
        Action keyYPressed = builder.sendKeys("y").build();
        webDriver.get("http://google.com/");
        webDriver.findElement(By.name("q")).sendKeys("sdasdasd");
        webDriver.findElement(By.name("q")).clear();;
        keyPPressed.perform();
        keyDPressed.perform();
        keyDPressed.perform();
        keyDotPressed.perform();
        keyBPressed.perform();
        keyYPressed.perform();
        webDriver.findElement(By.name("q")).sendKeys(Keys.RETURN);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("g")));
        webSite.taskPage().openResult("Правила дорожного движения Республики Беларусь");

        System.out.println(webDriver.getTitle());
        System.out.println(webDriver.getCurrentUrl());

    }
    @Test
    public void testPDD(){
        webSite.mainPage().pddClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/ru/"));
        webSite.pddPage().pddByClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/by/"));
        webSite.pddPage().pddEnClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/en/"));
        webSite.pddPage().pddAmdroidClock();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/android/"));
        webSite.pddPage().pddIphoneClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/iphone/"));
        webSite.pddPage().pddFbClock();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/fb/"));
        webSite.pddPage().pddStageClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/%D1%81%D1%82%D0%B0%D0%B6/"));
        webSite.pddPage().pddRespClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/resp/"));
        webSite.pddPage().pddCrimeClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/crime/"));
        webSite.pddPage().pddHistoryClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/"));
                webSite.pddPage().History1920Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/moskva-1920/"));
                webSite.pddPage().History2006Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-03-06/"));
                webSite.pddPage().History2011Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-11/"));
                webSite.pddPage().History2012Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-12/"));
                webSite.pddPage().History2013Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-13/"));
                webSite.pddPage().History2014Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-14/"));
                webSite.pddPage().History2015Click();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-15/"));
        webSite.mainPage().pddClick();
        webSite.pddPage().pddHistory15Click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/history/changes-15/"));
    }
    @Test
    public void  testTests(){
        webSite.mainPage().testsClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("/tasks/"));
                webSite.testsPage().pddTasksTxtClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/tasks/online/"));
                webSite.mainPage().testsClick();
                webSite.testsPage().pddTestsTxtClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/tasks/android-test/"));
                webSite.mainPage().testsClick();
                webSite.testsPage().pddBullet();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/tasks/online/"));
                webSite.mainPage().testsClick();
                webSite.testsPage().pddMotoClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/moto/"));
                webSite.mainPage().testsClick();
                webSite.testsPage().pddTests();
                webSite.testsPage().playMarket();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("https://play.google.com/store/apps/details?id=by.pdd.tasks.test"));
    }
    @Test
    public void  testTraining() throws InterruptedException {
        webSite.mainPage().trainingClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("/educ/"));
                webSite.schoolPage().featuresClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/#1"));
                webSite.schoolPage().apperanceClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/#2"));
                webSite.schoolPage().minSysClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/#3"));
                webSite.schoolPage().educDemoClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/demo/"));
                webSite.schoolPage().educFullClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/full/"));
                webSite.schoolPage().educBuyClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/buy/"));
                webSite.schoolPage().educSupportClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/support/"));
                webSite.mainPage().trainingClick();
                webSite.schoolPage().crimeClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/crime/"));
                webSite.mainPage().trainingClick();
                webSite.schoolPage().respClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/resp/"));
                webSite.mainPage().trainingClick();
                webSite.schoolPage().licenseClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/license/"));
                webSite.mainPage().trainingClick();
                webSite.schoolPage().cardClick();
                webSite.schoolPage().waitForOpenImg();
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/educ/buy/']"));
//                webSite.schoolPage().waitForOpenImg();
                webSite.schoolPage().closeImgClick();
                webSite.schoolPage().waitForCloseImg();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/"));
                webSite.schoolPage().allSoftClick();
                Assert.assertTrue(webDriver.getCurrentUrl().contains("https://allsoft.by/product/53209/"));
    }
    @Test
    public void  testDisk() throws InterruptedException {
        webSite.mainPage().diskPddClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("/cd/"));
        webSite.diskPddPage().cdBuyClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/buy/"));
        webSite.diskPddPage().cdSupportClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/support/"));
        webSite.diskPddPage().cdClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/"));
        webSite.diskPddPage().educClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/educ/"));
        webSite.mainPage().diskPddClick();
        webSite.diskPddPage().crimeClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/crime/"));
        webSite.mainPage().diskPddClick();
        webSite.diskPddPage().respClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/pdd/resp/"));
        webSite.mainPage().diskPddClick();
        webSite.diskPddPage().licenseClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/license/"));
        webDriver.get("http://pdd.by/cd/");
        webSite.diskPddPage().cdBuyClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/buy/"));
        webSite.diskPddPage().cdClick();
        webSite.diskPddPage().supportClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/cd/support"));
    }
    @Test
    public void  testForChildren(){
        webSite.mainPage().ForChildrenClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/"));
        webSite.forChildrenPage().schoolClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/"));
        webSite.forChildrenPage().schoolDownloadClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/download/"));
        webSite.forChildrenPage().schoolBuyClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/buy/"));
        webSite.forChildrenPage().schoolSupportClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/support/"));
        webSite.forChildrenPage().descriptionClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/"));
        webSite.forChildrenPage().ozzClick();
        webDriver.get("http://pdd.by/");
        webSite.mainPage().ForChildrenClick();
        webSite.forChildrenPage().youngClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D1%8E%D0%BD%D1%8B%D0%B9-%D1%80%D0%B5%D0%B3%D1%83%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D1%89%D0%B8%D0%BA/"));
        webSite.forChildrenPage().pddForschoolClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/school/"));
        webSite.mainPage().ForChildrenClick();
        webSite.forChildrenPage().youIDClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0-%D1%8E%D0%B8%D0%B4/"));
        webSite.forChildrenPage().forCyclistClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D0%B2%D0%B5%D0%BB%D0%BE-%D0%BA%D0%BE%D0%BD%D0%BA%D1%83%D1%80%D1%81/"));
        webSite.forChildrenPage().pddForCyclistClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/events/2014-08-19/%D0%B2%D0%B5%D0%BB%D0%BE-%D0%BF%D0%B4%D0%B4/"));
        webSite.mainPage().ForChildrenClick();
        webSite.forChildrenPage().youngImgClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D1%8E%D0%BD%D1%8B%D0%B9-%D1%80%D0%B5%D0%B3%D1%83%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D1%89%D0%B8%D0%BA/"));
        webSite.mainPage().ForChildrenClick();
        webSite.forChildrenPage().youIDImgClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0-%D1%8E%D0%B8%D0%B4/"));
        webSite.mainPage().ForChildrenClick();
        webSite.forChildrenPage().forCyclistImgClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/%D0%BF%D0%B4%D0%B4-%D0%B4%D0%BB%D1%8F-%D0%B4%D0%B5%D1%82%D0%B5%D0%B9/%D0%B2%D0%B5%D0%BB%D0%BE-%D0%BA%D0%BE%D0%BD%D0%BA%D1%83%D1%80%D1%81/"));
    }
    @Test
    public void  testAutoSchool() throws InterruptedException {
        webSite.mainPage().autoSchoolClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("/schools/"));
        webSite.autoSchoolPage().vitebskClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B2%D0%B8%D1%82%D0%B5%D0%B1%D1%81%D0%BA/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().vitebskOblClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B2%D0%B8%D1%82%D0%B5%D0%B1%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().grodnoClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B3%D1%80%D0%BE%D0%B4%D0%BD%D0%BE/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().grodnoOnlClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B3%D1%80%D0%BE%D0%B4%D0%BD%D0%B5%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().minskClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%BC%D0%B8%D0%BD%D1%81%D0%BA/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().minskOblClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%BC%D0%B8%D0%BD%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().mogilevClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%BC%D0%BE%D0%B3%D0%B8%D0%BB%D0%B5%D0%B2/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().mogilevOblClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%BC%D0%BE%D0%B3%D0%B8%D0%BB%D0%B5%D0%B2%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().brestClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B1%D1%80%D0%B5%D1%81%D1%82/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().brestOblClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B1%D1%80%D0%B5%D1%81%D1%82%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().gomelClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B3%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C/"));
        webSite.mainPage().autoSchoolClick();
        webSite.autoSchoolPage().gomelOblClick();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("http://pdd.by/schools/%D0%B3%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F-%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/"));
    }
    @Test
    public void TaskPageTest() throws InterruptedException, IOException {
        webSite.mainPage().testsClick();
        webSite.testsPage().pddTasksTxtClick();
        webSite.taskPage().startClick();
        webSite.taskPage().waitForLoadingVariants();
        webSite.taskPage().var2Click();
        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0){
            webSite.taskPage().waitForClickable();
            webSite.taskPage().testNextClick();
            webSite.taskPage().waitForLoadingVariants();
            webSite.taskPage().var2Click();
            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                webSite.taskPage().waitForClickable();
                webSite.taskPage().testNextClick();
                webSite.taskPage().waitForLoadingVariants();
                webSite.taskPage().var1Click();
                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForLoadingVariants();
                    webSite.taskPage().var1Click();
                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForLoadingVariants();
                        webSite.taskPage().var1Click();
                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            System.out.println("Loos!");
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        System.out.println("Loos!");
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    System.out.println("Loos!");
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                System.out.println("Loos!");
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }
                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                            System.out.println("Loos!");
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForResult();
                            }
                        }
                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                        System.out.println("Loos!");
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForLoadingVariants();
                        webSite.taskPage().var1Click();
                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForResult();
                            }
                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForResult();
                        }
                    }
                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                    System.out.println("Loos!");
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForLoadingVariants();
                    webSite.taskPage().var1Click();
                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForLoadingVariants();
                        webSite.taskPage().var1Click();
                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForResult();
                            }
                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForResult();
                        }
                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForResult();
                    }
                }
            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                System.out.println("Loos 2!");
                webSite.taskPage().waitForClickable();
                webSite.taskPage().testNextClick();
                webSite.taskPage().waitForLoadingVariants();
                webSite.taskPage().var1Click();
                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForLoadingVariants();
                    webSite.taskPage().var1Click();
                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForLoadingVariants();
                        webSite.taskPage().var1Click();
                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForResult();
                            }
                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForResult();
                        }
                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForResult();
                    }
                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForResult();
                }
            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                webSite.taskPage().waitForClickable();
                webSite.taskPage().testNextClick();
                webSite.taskPage().waitForResult();
            }
        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
            System.out.println("Loos!");
            webSite.taskPage().waitForClickable();
            webSite.taskPage().testNextClick();
            webSite.taskPage().waitForLoadingVariants();
            webSite.taskPage().var1Click();
            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                webSite.taskPage().waitForClickable();
                webSite.taskPage().testNextClick();
                webSite.taskPage().waitForLoadingVariants();
                webSite.taskPage().var1Click();
                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForLoadingVariants();
                    webSite.taskPage().var1Click();
                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForLoadingVariants();
                        webSite.taskPage().var1Click();
                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForLoadingVariants();
                            webSite.taskPage().var1Click();
                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForLoadingVariants();
                                webSite.taskPage().var1Click();
                                if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForLoadingVariants();
                                    webSite.taskPage().var1Click();
                                    if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForLoadingVariants();
                                        webSite.taskPage().var1Click();
                                        if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForLoadingVariants();
                                            webSite.taskPage().var1Click();
                                            if(webDriver.findElements(By.xpath("//li[contains(@class,'m-corr')]")).size() > 0) {
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForLoadingVariants();
                                                webSite.taskPage().var1Click();
                                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                                webSite.taskPage().waitForClickable();
                                                webSite.taskPage().testNextClick();
                                                webSite.taskPage().waitForResult();
                                            }
                                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                            webSite.taskPage().waitForClickable();
                                            webSite.taskPage().testNextClick();
                                            webSite.taskPage().waitForResult();
                                        }
                                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                        webSite.taskPage().waitForClickable();
                                        webSite.taskPage().testNextClick();
                                        webSite.taskPage().waitForResult();
                                    }
                                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                    webSite.taskPage().waitForClickable();
                                    webSite.taskPage().testNextClick();
                                    webSite.taskPage().waitForResult();
                                }
                            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                                webSite.taskPage().waitForClickable();
                                webSite.taskPage().testNextClick();
                                webSite.taskPage().waitForResult();
                            }
                        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                            webSite.taskPage().waitForClickable();
                            webSite.taskPage().testNextClick();
                            webSite.taskPage().waitForResult();
                        }
                    }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                        webSite.taskPage().waitForClickable();
                        webSite.taskPage().testNextClick();
                        webSite.taskPage().waitForResult();
                    }
                }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                    webSite.taskPage().waitForClickable();
                    webSite.taskPage().testNextClick();
                    webSite.taskPage().waitForResult();
                }
            }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
                webSite.taskPage().waitForClickable();
                webSite.taskPage().testNextClick();
                webSite.taskPage().waitForResult();
            }
        }else if(webDriver.findElements(By.xpath("//li[contains(@class,'m-wrng')]")).size() > 0){
            webSite.taskPage().waitForClickable();
            webSite.taskPage().testNextClick();
            webSite.taskPage().waitForResult();
        }

//        webDriver.findElement(By.id("")).getAttribute("b-result m-corr");
        File screen = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("c:\\Jav\\screenshot.png"));
        Assert.assertTrue(webDriver.findElement(By.cssSelector
                ("body > div.h-main > div.l-content > div.b-results.m-failed.m-type_0 > ul > li:nth-child(3)"))
                .getText().contains("2"));
    }

    @After
    public void postCondition() throws InterruptedException {
        Thread.sleep(3000);
        if(webDriver != null)
            webDriver.quit();
    }

}
