package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DiskPddPage {
    private WebDriver webDriver;

    public DiskPddPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//a[@href='/cd/']")
    WebElement Cd;
    @FindBy(xpath = "//a[@href='/cd/buy/']")
    WebElement CdBuy;
    @FindBy(xpath = "//a[@href='/cd/support/']")
    WebElement CdSupprot;
    @FindBy(xpath = "//a[@href='/educ/']")
    WebElement Educ;
    @FindBy(xpath = "//a[@href='/pdd/crime/']")
    WebElement Crime;
    @FindBy(xpath = "//a[@href='/pdd/resp/']")
    WebElement Resp;
    @FindBy(xpath = "//a[@href='/cd/license/']")
    WebElement License;
    @FindBy(xpath = "//a[@href='/cd/buy']")
    WebElement Buy;
    @FindBy(linkText = "службу технической поддержки")
    WebElement Supprot;

    public void cdClick(){
        Cd.click();
    }
    public void cdBuyClick() {
        CdBuy.click();
    }
    public void cdSupportClick(){
        CdSupprot.click();
    }
    public void educClick(){
        Educ.click();
    }
    public void crimeClick(){
        Crime.click();
    }
    public void respClick(){
        Resp.click();
    }
    public void licenseClick(){
        License.click();
    }
    public void buyClick(){
        Buy.click();
    }
    public void supportClick(){
        Supprot.click();
    }


}
