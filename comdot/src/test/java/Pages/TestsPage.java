package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestsPage {
   private WebDriver webDriver;


    public TestsPage (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(linkText ="Задачи по ПДД он-лайн")
    WebElement PddTasksTxt;
    @FindBy(linkText ="Тест ПДД для Android")
    WebElement PddTestsTxt;
            @FindBy(xpath ="//a[@href='https://play.google.com/store/apps/details?id=by.pdd.tasks.test']")
            WebElement PlayMarket;
    @FindBy(linkText ="Билеты он-лайн по ПДД Республики Беларусь")
    WebElement PddBullet;
    @FindBy(linkText ="Тест ПДД Республики Беларусь для Android")
    WebElement PddTests;
    @FindBy(linkText ="Учебная программа ПДД для мотоциклистов")
    WebElement PddMoto;

    public void pddTasksTxtClick(){
        PddTasksTxt.click();
    }
    public void pddTestsTxtClick(){
        PddTestsTxt.click();
    }
    public void playMarket(){
        PlayMarket.click();
    }
    public void pddBullet(){
        PddBullet.click();
    }
    public void pddTests(){
        PddTests.click();
    }
    public void pddMotoClick(){
        PddMoto.click();
    }
}
