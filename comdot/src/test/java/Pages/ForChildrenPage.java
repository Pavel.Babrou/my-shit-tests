package Pages;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.jws.WebResult;
import javax.swing.*;
import java.util.Set;

public class ForChildrenPage {
    private WebDriver webDriver;

    public ForChildrenPage (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath ="//a[@href='/school/']")
    WebElement School;
    @FindBy(linkText ="Программа ПДД для школьников (электронная версия и CD)")
    WebElement PddSchool;
    @FindBy (linkText = "Описание программы")
    WebElement Description;
            @FindBy(xpath ="//a[@href='/school/download/']")
            WebElement SchoolDownload;
            @FindBy(xpath ="//a[@href='/school/buy/']")
            WebElement SchoolBuy;
            @FindBy(xpath ="//a[@href='/school/support/']")
            WebElement SchoolSupport;
            @FindBy(xpath ="//a[@href='https://oz.by/edusoft/more10546607.html']")
            WebElement Ozz;
    @FindBy(linkText = "Юный регулировщик")
    WebElement Young;
    @FindBy(linkText = "Конкурсная программа «Юный регулировщик»")
    WebElement YoungImg;
            @FindBy(xpath = "//a[@href='/dl/pdd32_reg_setup_(demo).exe']")
            WebElement Install;
            @FindBy(linkText = "«Правила дорожного движения для школьников»")
            WebElement PddForSchool;
    @FindBy(linkText = "Программа для ЮИД")
    WebElement YouID;
    @FindBy(linkText = "Конкурсная программа для ЮИД")
    WebElement YouIDImg;
    @FindBy (xpath = "//a[@href='/пдд-для-детей/вело-конкурс/']")
    WebElement ForCyclist;
    @FindBy(linkText = "Конкурсная программа для велосипедистов")
    WebElement ForCyclistImg;
            @FindBy(xpath ="//a[@href='/events/2014-08-19/вело-пдд/']")
            WebElement PddForCyclist;

    public void schoolClick(){
        School.click();
    }
    public void pddSchoolClick(){
        PddSchool.click();
    }
    public void descriptionClick(){
        Description.click();
    }
            public void schoolDownloadClick(){
                SchoolDownload.click();
            }
            public void schoolBuyClick(){
                SchoolBuy.click();
            }
            public void schoolSupportClick(){
                SchoolSupport.click();
            }
            public void ozzClick(){
                String originalWinwow = webDriver.getWindowHandle();
                final Set<String> oldWindowSet = webDriver.getWindowHandles();
                Ozz.click();
                String newWinow = (new WebDriverWait(webDriver, 10))
                        .until(new ExpectedCondition<String>() {
                            public String apply(WebDriver driver){
                                Set<String> newWinowsSet = driver.getWindowHandles();
                                newWinowsSet.removeAll(oldWindowSet);
                                return newWinowsSet.size() > 0 ?
                                        newWinowsSet.iterator().next() : null;
                            }
                        }
                        );
                webDriver.switchTo().window(newWinow);
                System.out.println("New window title: " + webDriver.getTitle());
                webDriver.close();
                webDriver.switchTo().window(originalWinwow);
                System.out.println("Old window title: " + webDriver.getTitle());
            }
    public void youngClick(){
        Young.click();
    }
    public void youngImgClick(){
        YoungImg.click();
    }
            public void installClick(){
                Install.click();
            }
            public void pddForschoolClick(){
            PddForSchool.click();
            }
    public void youIDClick(){
        YouID.click();
    }
    public void youIDImgClick(){
        YouIDImg.click();
    }
    public void forCyclistClick(){
        ForCyclist.click();
    }
    public void forCyclistImgClick(){
        ForCyclistImg.click();
    }
            public void pddForCyclistClick(){
        PddForCyclist.click();
    }

}
