package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MainPage {
    private WebDriver webDriver;

    public MainPage(WebDriver driver){
        webDriver=driver;
        PageFactory.initElements(webDriver, this);
    }


//        @FindBy(css ="a[href*='pdd']")
//        @FindBy(linkText = "ПДД")
        @FindBy(xpath ="//a[@href='/pdd/']")
        WebElement PDD;
        @FindBy(linkText = "Тесты")
        WebElement Tests;
        @FindBy(linkText = "Учебная программа")
        WebElement Training;
        @FindBy(linkText = "Диск ПДД")
        WebElement DickPdd;
        @FindBy(linkText = "Для детей")
        WebElement ForChildren;
        @FindBy(linkText = "Автошколы")
        WebElement DrivingSchool;


    public void pddClick(){
        PDD.click();
    }
    public void testsClick(){
        Tests.click();
    }
    public void trainingClick(){
        Training.click();
    }
    public void diskPddClick(){
        DickPdd.click();
    }
    public void ForChildrenClick(){
        ForChildren.click();
    }
    public void autoSchoolClick(){
        DrivingSchool.click();
    }

}
