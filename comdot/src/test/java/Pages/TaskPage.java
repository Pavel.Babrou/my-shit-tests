package Pages;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

public class TaskPage {
    private WebDriver webDriver;
    private WebDriverWait wait;

    public TaskPage(WebDriver driver) {
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
        wait = new WebDriverWait(webDriver, 30);
    }

    Random random = new Random((System.currentTimeMillis()));
    int number = 0 + random.nextInt(4 - 0 + 1);


    @FindBy(id = "test-start")
    WebElement Start;
    @FindBy(id = "test-result")
    WebElement TestResult;
    @FindBy(id = "test-next")
    WebElement TestNext;
    @FindBy (css = "#variants > li:nth-child(1) > span")
    WebElement Var1;
    @FindBy (css = "#variants > li:nth-child(2) > span")
    WebElement Var2;
    @FindBy (css = "#variants > li:nth-child(3) > span")
    WebElement Var3;
    @FindBy (id = "result-panel")
    WebElement ResultPanel;
    @FindBy (css = "body > div.h-main > div.l-content > div.b-results.m-failed.m-type_0 > ul > li:nth-child(3)")
    WebElement Result;

    public void openResult(String siteTitle){
        webDriver.findElement(By.className("r")).click();
    }

    public void testNextClick(){
        TestNext.click();
    }
    public void var1Click(){
        Var1.click();
    }
    public void var2Click(){
        Var2.click();
    }
    public void var3Click(){
        Var3.click();
    }
    public void startClick() {
        Start.click();
    }

    public void waitForClickable(){
        wait.until(ExpectedConditions.elementToBeClickable(By.id("test-next")));
    }

    public void waitForResult(){
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(
                "body > div.h-main > div.l-content > div.b-results.m-failed.m-type_0 > ul > li:nth-child(3)")));
    }

    public void waitForLoadingVariants(){
        wait.until(ExpectedConditions.elementToBeClickable(By.id("variants")));
    }

    public void testResultInput() {
        TestResult.sendKeys("" + number);
    }

}