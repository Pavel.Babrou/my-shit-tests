package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PddPage {
    private WebDriver webDriver;

    public PddPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath ="//a[@href='/pdd/by/']")
    WebElement PddBy;
    @FindBy(xpath ="//a[@href='/pdd/en/']")
    WebElement PddEn;
    @FindBy(xpath ="//a[@href='/pdd/android/']")
    WebElement PddAndroid;
    @FindBy(xpath ="//a[@href='/pdd/iphone/']")
    WebElement PddIphone;
    @FindBy(xpath ="//a[@href='/pdd/fb/']")
    WebElement PddFb;
    @FindBy(xpath ="//a[@href='/pdd/стаж/']")
    WebElement PddStage;
    @FindBy(xpath ="//a[@href='/pdd/resp/']")
    WebElement PddResp;
    @FindBy(xpath ="//a[@href='/pdd/crime/']")
    WebElement PddCrime;
    @FindBy(xpath ="//a[@href='/pdd/history/']")
    WebElement PddHistory;
            @FindBy(xpath = "//a[@href='/pdd/history/moskva-1920/']" )
            WebElement PddHistory1920;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-03-06/']" )
            WebElement PddHistory2006;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-11/']" )
            WebElement PddHistory2011;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-12/']" )
            WebElement PddHistory2012;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-13/']" )
            WebElement PddHistory2013;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-14/']" )
            WebElement PddHistory2014;
            @FindBy(xpath = "//a[@href='/pdd/history/changes-15/']" )
            WebElement PddHistory2015;
    @FindBy(xpath ="//a[@href='/pdd/history/changes-15/']")
    WebElement PddHistory15;

    public void pddByClick(){
        PddBy.click();
    }
    public void pddEnClick(){
        PddEn.click();
    }
    public void pddAmdroidClock(){
        PddAndroid.click();
    }
    public void pddIphoneClick(){
        PddIphone.click();
    }
    public void pddFbClock(){
        PddFb.click();
    }
    public void pddStageClick(){
        PddStage.click();
    }
    public void pddRespClick(){
        PddResp.click();
    }
    public void pddCrimeClick(){
        PddCrime.click();
    }
    public void pddHistoryClick(){
        PddHistory.click();
    }
            public void History1920Click(){
                PddHistory1920.click();
            }
            public void History2006Click(){
                PddHistory2006.click();
            }
            public void History2011Click(){
                PddHistory2011.click();
            }
            public void History2012Click(){
                PddHistory2012.click();
            }
            public void History2013Click(){
                PddHistory2013.click();
            }
            public void History2014Click(){
                PddHistory2014.click();
            }
            public void History2015Click(){
                PddHistory2015.click();
            }
    public void pddHistory15Click(){
        PddHistory15.click();
    }

}
