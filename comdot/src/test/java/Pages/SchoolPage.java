package Pages;

import com.google.common.graph.MutableNetwork;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SchoolPage {

    private WebDriver webDriver;
    WebDriverWait wait;
    WebElement webElementWait;

    public SchoolPage (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
        wait = new WebDriverWait(webDriver, 30);
    }

    @FindBy(xpath = "//a[@href='/educ/']")
    WebElement Educ;
    @FindBy(xpath = "//a[@href='/educ/demo/']")
    WebElement EducDemo;
    @FindBy(xpath ="//a[@href='/educ/full/']")
    WebElement EducFull;
    @FindBy(xpath ="//a[@href='/educ/buy/']")
    WebElement EducBuy;
    @FindBy(xpath ="//a[@href='/educ/support/']")
    WebElement EducSupport;
    @FindBy(linkText ="Особенности программы")
    WebElement Features;
    @FindBy(linkText ="Внешний вид программы")
    WebElement Apperance;
    @FindBy(linkText ="Минимальные системные требования")
    WebElement MinSys;
    @FindBy(xpath ="//a[@href='/pdd/crime/']")
    WebElement Crime;
    @FindBy(xpath ="//a[@href='/pdd/resp/']")
    WebElement Resp;
    @FindBy(xpath ="//a[@href='/educ/license/']")
    WebElement License;
    @FindBy(xpath ="//a[@href='/img/educ/card-maket-vert-17.jpg']")
    WebElement Card;
    @FindBy(xpath ="//a[@href='https://allsoft.by/product/53209/']")
    WebElement AllSoft;
    @FindBy(id = "lightbox-secNav-btnClose")
    WebElement CloseImg;

    public void WebElementWait(){
        webElementWait = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("lightbox-secNav-btnClose")));
    }
    public void waitForCloseImg(){
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("lightbox-secNav-btnClose")));
    }
    public void waitForOpenImg(){
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div#jquery-lightbox a#lightbox-secNav-btnClose")));
    }

    public void educDemoClick(){
        EducDemo.click();
    }
    public void educFullClick(){
        EducFull.click();
    }
    public void educBuyClick(){
        EducBuy.click();
    }
    public void educSupportClick(){
        EducSupport.click();
    }
    public void featuresClick(){
        Features.click();
    }
    public void apperanceClick(){
        Apperance.click();
    }
    public void minSysClick(){
        MinSys.click();
    }
    public void crimeClick(){
        Crime.click();
    }
    public void respClick(){
        Resp.click();
    }
    public void licenseClick(){
        License.click();
    }
    public void cardClick(){
        Card.click();
    }
    public void allSoftClick(){
        AllSoft.click();
    }
    public void closeImgClick(){
        CloseImg.click();
    }

}
