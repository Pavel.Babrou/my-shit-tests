package Pages;

import org.openqa.selenium.WebDriver;

public class PddSite {
    WebDriver webDriver;

    public PddSite (WebDriver driver){
        webDriver = driver;
    }

    public MainPage mainPage(){
        return new MainPage(webDriver);
    }

    public PddPage pddPage(){
        return new PddPage(webDriver);
    }

    public TestsPage testsPage(){return new TestsPage(webDriver);}

    public SchoolPage schoolPage(){return new SchoolPage(webDriver);}

    public DiskPddPage diskPddPage(){return new DiskPddPage(webDriver);}

    public ForChildrenPage forChildrenPage(){return new ForChildrenPage(webDriver);}

    public AutoSchoolPage autoSchoolPage(){return new AutoSchoolPage(webDriver);}

    public TaskPage taskPage(){return new TaskPage(webDriver);}

    }
