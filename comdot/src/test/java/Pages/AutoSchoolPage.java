package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutoSchoolPage {

    private WebDriver webDriver;

    public  AutoSchoolPage (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(3) > a"))
    WebElement Vitebsk;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(4) > a"))
    WebElement VitebskObl;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(8) > a"))
    WebElement Grodno;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(7) > a"))
    WebElement GrodnoObl;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(9) > a"))
    WebElement Minsk;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(10) > a"))
    WebElement MinskObl;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(11) > a"))
    WebElement Mogilev;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(12) > a"))
    WebElement MogilevObl;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(1) > a"))
    WebElement Brest;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(2) > a"))
    WebElement BrestObl;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(5) > a"))
    WebElement Gomel;
    @FindBy(css = ("body > div.h-main > div.l-content > ul > li:nth-child(6) > a"))
    WebElement GomelObl;


    public void vitebskClick(){
        Vitebsk.click();
    }
    public void vitebskOblClick(){
        VitebskObl.click();
    }
    public void grodnoClick(){
        Grodno.click();
    }
    public void grodnoOnlClick(){
        GrodnoObl.click();
    }
    public void minskClick(){
        Minsk.click();
    }
    public void minskOblClick(){
        MinskObl.click();
    }
    public void mogilevClick(){
        Mogilev.click();
    }
    public void mogilevOblClick(){
        MogilevObl.click();
    }
    public void brestClick(){
        Brest.click();
    }
    public void brestOblClick(){
        BrestObl.click();
    }
    public void gomelClick(){
        Gomel.click();
    }
    public void gomelOblClick(){
        GomelObl.click();
    }
}
