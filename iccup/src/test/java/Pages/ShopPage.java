package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShopPage {
    WebDriver webDriver;

    public ShopPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy (xpath = "//*[@id=\"level0\"]/div[3]/div/a")
    WebElement Sacr;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[1]/div[1]/a/span[1]/img")
    WebElement ProAccount;
    @FindBy (xpath = "//*[@id=\"buyAcc\"]/tbody/tr[5]/td/input")
    WebElement BuyProAcoount;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[1]/div[2]/a/span[1]/img")
    WebElement LadderArmor;
    @FindBy (xpath = "//*[@id=\"shop-form\"]/div/div/table/tbody/tr[5]/td/input")
    WebElement BuyLaddeArmor;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[1]/div[3]/a/span[1]/img")
    WebElement LadderBonus;
    @FindBy (xpath = "//*[@id=\"shop-form\"]/div/div/table/tbody/tr[5]/td/input")
    WebElement BuyLadderBonus;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div[1]/div[1]/a/div/img")
    WebElement AllinPack;
    @FindBy (xpath = "//*[@id=\"buyAcc\"]/tbody/tr[5]/td/input")
    WebElement BuyAllInPack;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div[1]/div[2]/a/div/img")
    WebElement DefPack;
    @FindBy (xpath = "//*[@id=\"buyAcc\"]/tbody/tr[5]/td/input")
    WebElement BuyDefPack;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[1]/td[1]/a")
    WebElement VisasPay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[1]/td[2]/a")
    WebElement QiwiPay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[1]/td[3]/a")
    WebElement MobilePay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[1]/td[3]/a")
    WebElement YandexPay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[2]/td[2]/a")
    WebElement WebMoneyPay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[2]/td[3]/a")
    WebElement AlphaBankPay;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[4]/div[2]/div/div[2]/table/tbody/tr[3]/td/a")
    WebElement SmsPay;
    @FindBy (xpath = "//*[@id=\"submitForm\"]/span[1]/a")
    WebElement Back;

}
