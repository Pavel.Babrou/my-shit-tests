package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DotaPage {
    private WebDriver webDriver;

    public DotaPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy (xpath = "//*[@id=\"main\"]/div[1]/div[2]/a[1]")
    WebElement StartPlay;
    @FindBy (xpath = "//*[@id=\"main\"]/div[1]/div[2]/a[2]")
    WebElement DownloadLauncher;
    @FindBy (xpath = "//*[@id=\"main\"]/div[1]/div[2]/a[3]")
    WebElement DownloadDota;
    @FindBy (xpath = "//*[@id=\"main\"]/div[1]/div[2]/h3/a")
    WebElement GameScreen;
    @FindBy (xpath = "//*[@id=\"main\"]/div[1]/div[1]/a")
    WebElement KnowMore;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/ul/li[2]/span/a")
    WebElement WhenSeasonEnds;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/ul/li[3]/span/a")
    WebElement HowWorkRate;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/ul/li[4]/span/a")
    WebElement TeamForPlay;
}
