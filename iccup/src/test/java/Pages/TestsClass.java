package Pages;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestsClass {
    WebDriver webDriver;
    Site webSite;
    WebDriverWait wait;

    @Before
        public void preCondition() {
            System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Maven\\chromedriver.exe");
            webDriver = new ChromeDriver();
            wait = new WebDriverWait(webDriver, 30, 300);
            webSite = new Site(webDriver);
            webDriver.manage().window().maximize();
            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
            webDriver.get("https://iccup.com/login.html");

        }
    /****************** SIGN IN & SIGN UP ***************** */

//    @Test
//    public void SignInValidValues(){
//        webSite.mainPage().LoginField.clear();
//        webSite.mainPage().LoginField.sendKeys("Riverrise");
//        webSite.mainPage().PassField.click();
//        webSite.mainPage().PassField.sendKeys("winda7");
//        webSite.mainPage().EnterButton.click();
//        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/profile/view/riverrise.html"));
//    }
//    @Test
//    public void SignInInvalidValues(){
//        webSite.mainPage().LoginField.clear();
//        webSite.mainPage().LoginField.sendKeys("dfghjdsdfgeg");
//        webSite.mainPage().PassField.click();
//        webSite.mainPage().PassField.sendKeys("dgasdgwerefewr");
//        webSite.mainPage().EnterButton.click();
//        Assert.assertTrue(webSite.mainPage().WrongPass.isDisplayed());
//    }
//    @Test
//    public void SignUpInValidEmail(){
//        webSite.mainPage().Registration.click();
//        webSite.mainPage().NewLoginField.sendKeys("12345");
//        webSite.mainPage().NewPassField.sendKeys("qwe123");
//        webSite.mainPage().NewPassSecondField.sendKeys("qwe1234");
//        webSite.mainPage().EmailField.sendKeys("qwert12345mail.ru");
//        webSite.mainPage().Registrationbutton.click();
//        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Этот логин занят, выберите другой"));
//        Assert.assertTrue(webSite.mainPage().WrongSecondPassMessage.getText().contains("Введённые пароли должны совпадать."));
//        Assert.assertTrue(webSite.mainPage().WrongEmailMessage.getText().contains("Введите правильный адрес."));
//    }
//    @Test
//    public void SignUpMaxValues(){
//        webSite.mainPage().Registration.click();
//        webSite.mainPage().NewLoginField.sendKeys("qqqqqqqqqqqqqqq");
//        webSite.mainPage().NewPassField.sendKeys("123");
//        webSite.mainPage().NewPassSecondField.sendKeys("123");
//        webSite.mainPage().EmailField.sendKeys("qwertmail.ru");
//        webSite.mainPage().Registrationbutton.click();
//        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Используйте буквы в диапазоне (a-z) и цифры (0-9)"));
//        Assert.assertTrue(webSite.mainPage().WrongPassMessage.getText().contains("Пароль должен содержать минимум 6 символов"));
//        Assert.assertTrue(webSite.mainPage().WrongEmailMessage.getText().contains("Введите правильный адрес."));
//    }
    @Test
    public void SignUpMinValues(){
        webSite.mainPage().Registration.click();
        webSite.mainPage().NewLoginField.sendKeys("q");
        webSite.mainPage().NewPassField.sendKeys("qwert12345");
        webSite.mainPage().NewPassSecondField.sendKeys("qwert12345");
        webSite.mainPage().EmailField.sendKeys("qwert12345mail.ru");
        webSite.mainPage().Registrationbutton.click();
        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Используйте буквы в диапазоне (a-z) и цифры (0-9)"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"rega\"]/div[8]/h2/span")).isDisplayed());
    }
    @Test
    public void SignUpValidEmail(){
        webSite.mainPage().Registration.click();
        webSite.mainPage().NewLoginField.sendKeys("q");
        webSite.mainPage().NewPassField.sendKeys("qwert12345");
        webSite.mainPage().NewPassSecondField.sendKeys("qwert12345");
        webSite.mainPage().EmailField.sendKeys("qwert12345@mail.ru");
        webSite.mainPage().Registrationbutton.click();
        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Используйте буквы в диапазоне (a-z) и цифры (0-9)"));
        Assert.assertFalse(webDriver.findElements(By.xpath("//*[@id=\"rega\"]/div[8]/h2/span")).size() > 0);
    }

    @Test
    public void SignUpEmptyField(){
        webSite.mainPage().Registration.click();;
        webSite.mainPage().NewLoginField.clear();
        webSite.mainPage().NewPassField.clear();
        webSite.mainPage().NewPassSecondField.clear();
        webSite.mainPage().EmailField.clear();
        webSite.mainPage().Registrationbutton.click();
        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Используйте буквы в диапазоне (a-z) и цифры (0-9)"));
        Assert.assertTrue(webSite.mainPage().WrongPassMessage.getText().contains("Пароль должен содержать минимум 6 символов"));
        Assert.assertTrue(webSite.mainPage().WrongSecondPassMessage.getText().contains("Введённые пароли должны совпадать."));
        Assert.assertTrue(webSite.mainPage().WrongEmailMessage.getText().contains("Введите правильный адрес."));
    }
    /****************** SEARCH ***************** */
    @Test
    public void SearchByName(){
        String name = "Riverrise";
        webSite.mainPage().SearchByName.clear();
        webSite.mainPage().SearchByName.sendKeys(name);
        webSite.mainPage().SearchButton.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"profile\"]/div[3]/h1"))
                .getText().contains("Результаты поиска: '" + name +"'"));
    }

    /****************** MAIN PAGE BOTTOM LINKS ***************** */

    @Test
    public void MinPageBottomLinks(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().MainPageLink.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/"));
        webSite.mainPage().PlayStart.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Как начать играть в доту?"));
        webSite.mainPage().Partners.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Наши партнеры"));
        webSite.mainPage().Development.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Вакансии команды разработчиков"));
        webSite.mainPage().Bans.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h2")).getText().contains("Баны и причины"));
        webSite.mainPage().Forum.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"forum\"]/div[3]/div[3]/span")).getText().contains("iCCup форумы"));
        webSite.mainPage().Ad.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Реклама на iCCup"));
        webSite.mainPage().Admins.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("iCCup Staff"));
        webSite.mainPage().Contacts.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Контакты"));
        webSite.mainPage().Conditions.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Terms of use"));
    }

    /****************** MAIN PAGE TOP LINKS ***************** */

    @Test
    public void MainPageTopLinks(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().MainPageLink.click();
        webSite.mainPage().Games.click();
        webSite.mainPage().StarcradtGame.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"right\"]/div/div[2]/ul/li[2]/span/span[2]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"right\"]/div/div[2]/ul/li[2]/span/span[2]/a")).getAttribute("href").contains("/files/view/Anti-Hack_Launcher_v1.4_build_88_02.html"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"right\"]/div/div[2]/ul/li[3]/span/span[2]/a")).getAttribute("href").contains("/files/view/iCCup_reg_file.html"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"right\"]/div/div[2]/ul/li[4]/span/span[2]/a")).getAttribute("href").contains("/files/view/iCCup_maps.html"));

        /****************** STARCRAFT PAGE ***************** */

        webSite.starCraftPage().HowToPlayStart.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/h3[1]/strong/span/span")).getText().contains("Где скачать игру?"));
        webDriver.navigate().back();
        webSite.starCraftPage().WhenEndSeason.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("StarCraft Season"));
        webDriver.navigate().back();
        webSite.starCraftPage().RateSystem.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Starcraft Rating System"));
        webDriver.navigate().back();
        webSite.starCraftPage().RateMaps.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("StarCraft Ladder Maps"));
        webDriver.navigate().back();
        webSite.starCraftPage().Tourney.click();
        webSite.starCraftPage().MakeTournye.click();
        Assert.assertTrue(webDriver.findElement(By.name("registration")).isDisplayed());
        webSite.mainPage().NewsPage.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[2]/h2/a")).getText().contains("Конкурсы и их итоги"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"right\"]/div/div[2]/h2/a")).isDisplayed());
        webSite.mainPage().ForumTop.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"forum\"]/div[3]/div[3]/span")).getText().contains("iCCup форумы"));
        webSite.mainPage().Rate.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"profile\"]/div[1]/h1")).getText().contains("Рейтинг"));
        webSite.mainPage().File.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[3]/ul/li[2]/a/span")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[3]/ul/li[1]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[4]/div/span/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[6]/div/span/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[8]/div/span/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[10]/div/span/a")).isDisplayed());
    }

    /****************** RULES PAGE ***************** */

    @Test
    public void RulesPage(){
        webSite.mainPage().Rules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила"));
        webSite.mainPage().BasicRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила поведения на сервере"));
        webDriver.navigate().back();
        webSite.mainPage().ForumRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила Форума"));
        webDriver.navigate().back();
        webSite.mainPage().BasicRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).isDisplayed());
        webDriver.navigate().back();
        webSite.mainPage().StatsComeBack.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Условия возврата игровой статистики"));
        webDriver.navigate().back();
        webSite.mainPage().LadderRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила ладдера"));
        webDriver.navigate().back();
        webSite.mainPage().ReplayRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила реплеев"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/p[1]/img")).isDisplayed());
        webDriver.navigate().back();
        webSite.mainPage().TourneyRules.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Правила турниров"));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/h3[1]/img")).isDisplayed());
    }

    /****************** LEAGUE PAGE ***************** */

    @Test
    public void LeaguePage(){
        webSite.mainPage().League.click();
        webSite.mainPage().DotaLeague.click();
        webSite.mainPage().System.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Система проведения ICCup Dota League"));
        webDriver.navigate().back();
        webSite.mainPage().Invites.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Приглашённые команды на ICCup Dota League"));
        webDriver.navigate().back();
        webSite.mainPage().Structure.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Список команд ICCup Dota League"));
        webDriver.navigate().back();
        webSite.mainPage().Regulation.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Регламент iCCup Dota League"));
        webDriver.navigate().back();
        webSite.mainPage().PrizeFund.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Призовой фонд iCCup Dota League"));
        webDriver.navigate().back();
        webSite.mainPage().TournamentGrid.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[1]/h1")).getText().contains("Сетка и расписание IDL"));
    }

    /****************** REPLAYS PAGE ***************** */

    @Test
    public void ReplayPage() throws IOException {
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().Replays.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"profile\"]/div[1]/h1")).isDisplayed());
        webSite.mainPage().SearchNameField.clear();
        webSite.mainPage().SearchNameField.sendKeys("qwe");
        webSite.mainPage().SearchMap.clear();
        webSite.mainPage().SearchMap.sendKeys("dota");
        webSite.mainPage().SearchReplayBtn.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"profile\"]/div[3]")).isDisplayed());
        webSite.mainPage().DownloadReplay.click();
        webSite.mainPage().NameField.clear();
        webSite.mainPage().RegBtn.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Заполните поля формы"));
        webSite.mainPage().NameField.sendKeys("qwe");
        webSite.mainPage().DescriptionField.sendKeys("qwerty");
        webSite.mainPage().RegBtn.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Не выбран файл для загрузки"));
        webDriver.findElement(By.xpath("//*[@id=\"uploadBug\"]/div[2]/span")).click();;
        File screen = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("c:\\Jav\\screenshot.png"));
    }

    /****************** DOTA PAGE ***************** */

    @Test
    public void DotaPAge(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().DotaPage.click();
        webSite.dotaPage().StartPlay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/p[2]/span[1]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/p[2]/span[2]/a")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().DownloadLauncher.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[3]/ul/li[2]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"left\"]/div/div[3]/ul/li[1]/a")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().DownloadDota.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/h3[2]")).getText().contains("Официальная карта: сейчас стоит на ботах"));
        webDriver.navigate().back();
        webSite.dotaPage().WhenSeasonEnds.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/table/tbody")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().HowWorkRate.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/table/tbody")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().TeamForPlay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().KnowMore.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/p[2]/span[1]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div[2]/p[2]/span[2]/a")).isDisplayed());
        webDriver.navigate().back();
        webSite.dotaPage().GameScreen.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"static-2\"]/div")).isDisplayed());
    }

    /****************** SHOP PAGE ***************** */

    @Test
    public void ShopPage(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().ShopPage.click();
        webSite.shopPage().ProAccount.click();
        webSite.shopPage().BuyProAcoount.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/store/category/6.html"));
        webSite.shopPage().LadderArmor.click();
        webSite.shopPage().BuyLaddeArmor.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/store/category/6.html"));
        webSite.shopPage().LadderBonus.click();
        webSite.shopPage().BuyLadderBonus.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/store/category/6.html"));
        webSite.shopPage().DefPack.click();
        webSite.shopPage().BuyDefPack.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/store/category/6.html"));
        webSite.shopPage().AllinPack.click();
        webSite.shopPage().BuyAllInPack.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/store/category/6.html"));
        webSite.shopPage().Sacr.click();
        webSite.shopPage().VisasPay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webDriver.navigate().back();
        webSite.shopPage().QiwiPay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webSite.shopPage().Back.click();
        webSite.shopPage().MobilePay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webDriver.navigate().back();
        webSite.shopPage().YandexPay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webDriver.navigate().back();
        webSite.shopPage().WebMoneyPay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webDriver.navigate().back();
        webSite.shopPage().AlphaBankPay.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"submitForm\"]/span[2]/input")).isDisplayed());
        webDriver.navigate().back();
        webSite.shopPage().SmsPay.click();
        Assert.assertTrue(webDriver.findElement(By.id("country")).isDisplayed());
    }

    /****************** SHOP PAGE ***************** */

    @Test
    public void BugsPage(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().BugsPage.click();
        webSite.bugsPage().NewBugs.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"main\"]/div/div[3]/div[1]/div[3]/span")).getAttribute("class").contains("ticket-pending"));
        webSite.bugsPage().AcceptBugs.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"main\"]/div/div[3]/div[1]/div[3]/span")).getAttribute("class").contains("ticket-approved"));
        webSite.bugsPage().IrrelevantBugs.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"main\"]/div/div[3]/div[1]/div[3]/span")).getAttribute("class").contains("ticket-inactive"));
        webSite.bugsPage().ReportBug.click();
        webSite.bugsPage().TitleOfReport.clear();
        webSite.bugsPage().DescriptionOfReport.clear();
        webSite.bugsPage().SendReport.click();
        webSite.bugsPage().SendReport.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Заполните поля формы"));
        webSite.bugsPage().TitleOfReport.sendKeys("123");
        webSite.bugsPage().DescriptionOfReport.sendKeys("123");
        webSite.bugsPage().SendReport.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/bugtracker/newbug.html"));
    }

    /****************** REPORT PAGE ***************** */

    @Test
    public void ReportPage(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().ReportPage.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().Rate.click();
        webSite.supportPage().Disconnect.click();
        webSite.supportPage().SelectGame.click();
        webSite.supportPage().TitleField.sendKeys("123");
        webSite.supportPage().DescriptionDield.sendKeys("123");
        webSite.supportPage().SendReportByLoad.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Не выбран файл для загрузки"));
        webSite.supportPage().TitleField.clear();
        webSite.supportPage().DescriptionDield.clear();
        webSite.supportPage().SendReportByLoad.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Заполните поля формы"));
        webSite.supportPage().Main.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().Rate.click();
        webSite.supportPage().Transfer.click();
        webSite.supportPage().TitleField.clear();
        webSite.supportPage().DescriptionDield.clear();
        webSite.supportPage().SendReport.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/div[1]/div/ul/li/span")).getText().contains("Не все поля заполнены"));
        webSite.supportPage().Main.click();
        webSite.supportPage().Support.click();
        webSite.supportPage().TitleField.sendKeys("qwe");
        webSite.supportPage().DescriptionDield.sendKeys("qwe");
        webSite.supportPage().SendReport.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/div[1]/div/ul/li/span")).getText().contains("Не все поля заполнены"));
    }

    @Test
    public void ShopAndCaps(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().ReportPage.click();
        webSite.supportPage().Shop.click();
        webSite.supportPage().NotCome.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[3]/div/button")).isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().DontWork.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[3]/div/button")).isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().TransferCoin.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[3]/div/button")).isDisplayed());
        webSite.supportPage().TitleField.sendKeys("qwe");
        webSite.supportPage().DescriptionDield.sendKeys("qwe");
        webSite.supportPage().SendReport.click();;
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/div[1]/div/ul/li/span")).getText().contains("Не все поля заполнены"));
    }
    @Test
    public void DotaReports(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        webSite.mainPage().ReportPage.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().Spam.click();
        webSite.supportPage().InGame.click();
        webSite.supportPage().SelectGame.click();
        webSite.supportPage().TitleField.sendKeys("qwe");
        webSite.supportPage().DescriptionDield.sendKeys("qwe");
        webSite.supportPage().SendReportByLoad.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"error-zone\"]")).getText().contains("Не выбран файл для загрузки"));
        webSite.supportPage().Main.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().ReportGameWiolation.click();
        webSite.supportPage().ReportFeed.click();
        Assert.assertTrue(webSite.supportPage().SelectGame.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().Hack.click();
        Assert.assertTrue(webSite.supportPage().SelectGame.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().Abuse.click();
        Assert.assertTrue(webSite.supportPage().SendReport.isDisplayed());
        webSite.supportPage().Main.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().Spam.click();
        webSite.supportPage().InGame.click();
        Assert.assertTrue(webSite.supportPage().SelectGame.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().InsultInPM.click();
        Assert.assertTrue(webSite.supportPage().SendReport.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().Lobby.click();
        Assert.assertTrue(webSite.supportPage().SendReport.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().SpamMaps.click();
        Assert.assertTrue(webSite.supportPage().SelectGame.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().BannedName.click();
        Assert.assertTrue(webSite.supportPage().SendReport.isDisplayed());
        webSite.supportPage().Main.click();
        webSite.supportPage().DotaReports.click();
        webSite.supportPage().Appeal.click();
        Assert.assertTrue(webSite.supportPage().SendReport.isDisplayed());
        webDriver.navigate().back();
        webSite.supportPage().AcountRecovery.click();
        webSite.supportPage().TitleField.sendKeys("qwe");
        webSite.supportPage().DescriptionDield.sendKeys("qwe");
        webSite.supportPage().SendReport.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"level0\"]/div[7]/div/div[1]/div/ul/li/span")).getText().contains("Не все поля заполнены"));
    }

    @After
    public void postCondition() throws InterruptedException {
        Thread.sleep(3000);
        if(webDriver != null)
            webDriver.quit();
    }
}
