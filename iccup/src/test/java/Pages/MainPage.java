package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.xml.xpath.XPath;
import java.util.logging.XMLFormatter;

public class MainPage {
    private WebDriver webDriver;

    public MainPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    /****************** PAGES ***************** */

    @FindBy (xpath = "//*[@id=\"level0\"]/div[5]/ul/li[2]/a")
    WebElement DotaPage;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[5]/ul/li[7]/a")
    WebElement ShopPage;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[5]/ul/li[5]/a")
    WebElement BugsPage;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[5]/ul/li[6]/a")
    WebElement ReportPage;


    /****************** SIGN IN & SIGN UP ***************** */

    @FindBy(xpath = "//div[@class='input']//h2//input[@type='text']")
    WebElement LoginField;
    @FindBy(xpath = "//div[@class='input']//h2//input[@type='password']")
    WebElement PassField;
    @FindBy (id = "postbut")
    WebElement EnterButton;
    @FindBy (css = "div.level0:nth-child(5) div.col-left div:nth-child(2) div.error-3:nth-child(2) div.mess-y div.mess-2 > ul:nth-child(1)")
    WebElement WrongPass;
    @FindBy (xpath = "//a[@href='/register.html'][contains(text(),'Регистрация')]")
    WebElement Registration;
    @FindBy (xpath = "//html//div[2]/h2[1]/input[1]")
    WebElement NewLoginField;
    @FindBy (xpath = "//html//div[4]/h2[1]/input[1]")
    WebElement NewPassField;
    @FindBy (xpath = "//html//div[6]/h2[1]/input[1]")
    WebElement NewPassSecondField;
    @FindBy (xpath = "//html//div[8]/h2[1]/input[1]")
    WebElement EmailField;
    @FindBy (xpath = "//input[@id='postbut']")
    WebElement Registrationbutton;
    @FindBy (xpath = "//*[@id=\"rega\"]/div[2]/h3")
    WebElement WrongLoginMessage;
    @FindBy (xpath = "//*[@id=\"rega\"]/div[4]/h3")
    WebElement WrongPassMessage;
    @FindBy (xpath = "//*[@id=\"rega\"]/div[6]/h3")
    WebElement WrongSecondPassMessage;
    @FindBy (xpath = "//*[@id=\"rega\"]/div[8]/h3")
    WebElement WrongEmailMessage;

    /****************** SEARCH ***************** */

    @FindBy (xpath = "//*[@id=\"menu0\"]/div/form/div/input")
    WebElement SearchByName;
    @FindBy (xpath = "//*[@id=\"menu0\"]/div/form/button")
    WebElement SearchButton;

    /****************** BOTTOM LINKS ***************** */

    @FindBy (xpath = "//*[@id=\"level0\"]/div[5]/ul/li[1]/a")
    WebElement MainPageLink;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[1]/a")
    WebElement PlayStart;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[5]/a")
    WebElement Partners;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[2]/a")
    WebElement Development;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[6]/a")
    WebElement Bans;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[3]/a")
    WebElement Forum;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[7]/a")
    WebElement Ad;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[4]/a")
    WebElement Admins;
    @FindBy (xpath = "//*[@id=\"footer\"]/ul/li[8]/a")
    WebElement Contacts;
    @FindBy (xpath = "//*[@id=\"footer\"]/div[4]/span[1]/a")
    WebElement Conditions;
    @FindBy (xpath = "//*[@id=\"footer\"]/div[4]/span[2]/a")
    WebElement VK;

    /****************** TOP LINKS ***************** */
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[2]/div/span")
    WebElement Games;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[2]/ul/li[2]/div/a")
    WebElement StarcradtGame;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[4]/div/a")
    WebElement NewsPage;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[6]/div/a")
    WebElement ForumTop;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[8]/div/a")
    WebElement Rate;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[10]/div/a")
    WebElement File;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[12]/div/a")
    WebElement Rules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/strong[1]/a[1]")
        WebElement BasicRules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/strong[1]/a[2]")
        WebElement ForumRules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/strong[1]/a[3]")
        WebElement SiteRules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/a[1]")
        WebElement StatsComeBack;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/a[2]")
        WebElement LadderRules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/a[3]")
        WebElement ReplayRules;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/p[1]/a[4]")
        WebElement TourneyRules;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[14]/div/span")
    WebElement League;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[1]/td[1]/a/img")
        WebElement System;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[1]/td[2]/a/img")
        WebElement Invites;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[1]/td[3]/a/img")
        WebElement Structure;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[2]/td/a[1]/img")
        WebElement Regulation;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[2]/td/a[2]/img")
        WebElement PrizeFund;
        @FindBy (xpath = "//*[@id=\"static-2\"]/div[2]/table/tbody[2]/tr[3]/td/a/img")
        WebElement TournamentGrid;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[14]/ul/li[2]/div/a")
    WebElement DotaLeague;
    @FindBy (xpath = "//*[@id=\"lvl-0\"]/li[18]/div/a")
    WebElement Replays;
        @FindBy (xpath = "//*[@id=\"form\"]/div[2]/a")
        WebElement DownloadReplay;
        @FindBy (id = "name")
        WebElement NameField;
        @FindBy (id = "description")
        WebElement DescriptionField;
        @FindBy (id = "reg-btn")
        WebElement RegBtn;
        @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/form/div[1]/h2[1]/input")
        WebElement SearchNameField;
        @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/form/div[1]/h2[2]/input")
        WebElement SearchMap;
        @FindBy (xpath = "//*[@id=\"right\"]/div/div[2]/form/div[2]/div[2]/input")
        WebElement SearchReplayBtn;

}

