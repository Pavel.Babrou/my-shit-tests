package Pages;

import org.openqa.selenium.WebDriver;

public class Site {
    WebDriver webDriver;

    public Site (WebDriver driver){
        webDriver = driver;
    }
    public MainPage mainPage (){
        return new MainPage(webDriver);
    }
    public DotaPage dotaPage(){
        return new DotaPage(webDriver);
    }
    public StarCraftPage starCraftPage(){
        return new StarCraftPage(webDriver);
    }
    public BugsPage bugsPage(){
        return new BugsPage(webDriver);
    }
    public SupportPage supportPage(){
        return new SupportPage(webDriver);
    }
    public ShopPage shopPage(){
        return new ShopPage(webDriver);
    }
}
