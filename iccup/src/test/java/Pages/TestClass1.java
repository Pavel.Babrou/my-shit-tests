package Pages;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestClass1 {
    WebDriver webDriver;
    Site webSite;
    WebDriverWait wait;

    @Before
    public void preCondition() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Maven\\chromedriver.exe");
        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 30, 300);
        webSite = new Site(webDriver);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        webDriver.get("http://the-internet.herokuapp.com/drag_and_drop");

    }

    @Test
    public void test() throws InterruptedException {

        WebElement element = webDriver.findElement(By.id("column-a"));

        WebElement target = webDriver.findElement(By.id("column-b"));

        (new Actions(webDriver)).dragAndDrop(element, target).perform();
        Thread.sleep(5000);


    }





    @Test
    public void SignInValidValues(){
        JavascriptExecutor js = (JavascriptExecutor)webDriver;
        WebElement element = (WebElement) js.executeScript("return jQuery.find('#q');");
        element.click();
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("Riverrise");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("winda7");
        webSite.mainPage().EnterButton.click();
        Assert.assertTrue(webDriver.getCurrentUrl().contains("https://iccup.com/profile/view/riverrise.html"));
    }
    @Test
    public void SignInInvalidValues(){
        webSite.mainPage().LoginField.clear();
        webSite.mainPage().LoginField.sendKeys("dfghjdsdfgeg");
        webSite.mainPage().PassField.click();
        webSite.mainPage().PassField.sendKeys("dgasdgwerefewr");
        webSite.mainPage().EnterButton.click();
        Assert.assertTrue(webSite.mainPage().WrongPass.isDisplayed());
    }
    @Test
    public void SignUpInValidEmail(){
        webSite.mainPage().Registration.click();
        webSite.mainPage().NewLoginField.sendKeys("12345");
        webSite.mainPage().NewPassField.sendKeys("qwe123");
        webSite.mainPage().NewPassSecondField.sendKeys("qwe1234");
        webSite.mainPage().EmailField.sendKeys("qwert12345mail.ru");
        webSite.mainPage().Registrationbutton.click();
        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Этот логин занят, выберите другой"));
        Assert.assertTrue(webSite.mainPage().WrongSecondPassMessage.getText().contains("Введённые пароли должны совпадать."));
        Assert.assertTrue(webSite.mainPage().WrongEmailMessage.getText().contains("Введите правильный адрес."));
    }
    @Test
    public void SignUpMaxValues(){
        webSite.mainPage().Registration.click();
        webSite.mainPage().NewLoginField.sendKeys("qqqqqqqqqqqqqqq");
        webSite.mainPage().NewPassField.sendKeys("123");
        webSite.mainPage().NewPassSecondField.sendKeys("123");
        webSite.mainPage().EmailField.sendKeys("qwertmail.ru");
        webSite.mainPage().Registrationbutton.click();
        Assert.assertTrue(webSite.mainPage().WrongLoginMessage.getText().contains("Используйте буквы в диапазоне (a-z) и цифры (0-9)"));
        Assert.assertTrue(webSite.mainPage().WrongPassMessage.getText().contains("Пароль должен содержать минимум 6 символов"));
        Assert.assertTrue(webSite.mainPage().WrongEmailMessage.getText().contains("Введите правильный адрес."));
    }
    @After
    public void postCondition() throws InterruptedException {
        Thread.sleep(3000);
        if(webDriver != null)
            webDriver.quit();
    }
}
