package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportPage {
    WebDriver webDriver;

    public SupportPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
    WebElement DotaReports;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
        WebElement Rate;
            @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
            WebElement Disconnect;
            @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
            WebElement Transfer;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[3]/a")
                WebElement Spam;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
                WebElement InsultInGame;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
                WebElement InsultInPM;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[3]/a")
                WebElement Lobby;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[4]/a")
                WebElement SpamMaps;
                @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[5]/a")
                WebElement BannedName;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
        WebElement ReportGameWiolation;
            @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
            WebElement ReportFeed;
            @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
            WebElement Hack;
            @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[3]/a")
            WebElement Abuse;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[4]/a")
        WebElement Appeal;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[5]/a")
        WebElement AcountRecovery;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/ul/li[3]/a")
    WebElement MyQuestion;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
    WebElement Support;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[3]/a")
    WebElement Shop;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
        WebElement NotCome;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[2]/a")
        WebElement DontWork;
        @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[3]/a")
        WebElement TransferCoin;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/div[4]/div[1]/div/div[5]/div[1]/a")
    WebElement SelectGame;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/ul/li[2]/a")
    WebElement Main;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[1]/div/input")
    WebElement TitleField;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[2]/div/textarea")
    WebElement DescriptionDield;
    @FindBy (id = "upload-btn")
    WebElement SendReportByLoad;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/form/fieldset/div[3]/div/button")
    WebElement SendReport;
    @FindBy (xpath = "//*[@id=\"level0\"]/div[7]/div/ul/li[1]/a")
    WebElement InGame;
}
