package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StarCraftPage {
    WebDriver webDriver;

    public StarCraftPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//*[@id=\"right\"]/div/div[3]/ul/li[1]/span/a")
    WebElement HowToPlayStart;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[3]/ul/li[2]/span/a[2]")
    WebElement WhenEndSeason;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[3]/ul/li[3]/span/a")
    WebElement RateSystem;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[3]/ul/li[4]/span/a")
    WebElement RateMaps;
    @FindBy (xpath = "//*[@id=\"right\"]/div/div[4]/h2/a")
    WebElement Tourney;
    @FindBy (xpath = "//*[@id=\"profile\"]/div[3]/ul/li/a")
    WebElement MakeTournye;
}
