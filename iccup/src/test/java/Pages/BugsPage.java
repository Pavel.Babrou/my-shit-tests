package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BugsPage {
    WebDriver webDriver;

    public BugsPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy (xpath = "//*[@id=\"main\"]/div/ul/li[1]/a")
    WebElement NewBugs;
    @FindBy (xpath = "//*[@id=\"main\"]/div/ul/li[2]/a")
    WebElement AcceptBugs;
    @FindBy (xpath = "//*[@id=\"main\"]/div/ul/li[3]/a")
    WebElement IrrelevantBugs;
    @FindBy (xpath = "//*[@id=\"main\"]/div/div[2]/a")
    WebElement ReportBug;
    @FindBy (xpath = "//*[@id=\"main\"]/div/ul/li[5]/a")
    WebElement ChangeList;
    @FindBy (xpath = "//*[@id=\"title\"]")
    WebElement TitleOfReport;
    @FindBy (id = "text")
    WebElement DescriptionOfReport;
    @FindBy (id = "reg-btn")
    WebElement SendReport;
}
